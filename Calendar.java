import java.net.URL;
import java.util.Scanner;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.awt.event.ActionEvent;
public class Calendar {
	public static void main(String[] args) {
		
		JFrame frame = new JFrame();
		frame.setTitle("Календар с празници");
		JPanel panel = new JPanel();
		panel.setBackground(Color.DARK_GRAY);
	    frame.setSize(646, 229);
	    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    frame.setContentPane(panel);
	    panel.setLayout(null);

	    JLabel imagelabel = new JLabel();
        URL imageUrl = Calendar.class.getResource("calendarche.png");
        ImageIcon imageTriangle = new ImageIcon(imageUrl);
        imagelabel.setIcon(imageTriangle);
        imagelabel.setBounds(403,18,192,159);
        panel.add(imagelabel);
        
        JLabel lblNewLabel_1 = new JLabel("Месец");
        lblNewLabel_1.setForeground(Color.WHITE);
        lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
        lblNewLabel_1.setBounds(-26, 18, 112, 24);
        panel.add(lblNewLabel_1);
        
        JLabel lblNewLabel_2 = new JLabel("Ден");
        lblNewLabel_2.setForeground(Color.WHITE);
        lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
        lblNewLabel_2.setBounds(-6, 68, 61, 24);
        panel.add(lblNewLabel_2);
        
        JTextField textM = new JTextField();
        textM.setBounds(65, 20, 86, 20);
        panel.add(textM);
        textM.setColumns(10);
        
        JTextField textD = new JTextField();
        textD.setBounds(65, 70, 86, 20);
        panel.add(textD);
        textD.setColumns(10);

        
        JButton btnNewButton = new JButton("Какво се е случило на тази дата?");
        btnNewButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        	
                String strM = textM.getText();
                String strD = textD.getText();
                
              	  if (strM.equals("")) {
              		 JOptionPane.showMessageDialog(frame, "Не може полето да остане празно", "Грешка", JOptionPane.ERROR_MESSAGE);
              	 } else if(strD.equals("")) {
              		 JOptionPane.showMessageDialog(frame, "Не може полето да остане празно", "Грешка", JOptionPane.ERROR_MESSAGE);
              	 } else {
                
             try { 
                 
                 int M = Integer.parseInt(strM);
                 int D = Integer.parseInt(strD);
                 
                	 if (M <= 0) {
                	 JOptionPane.showMessageDialog(frame, "Не трябва месеца да е по-малък или равен на 0", "Грешка", JOptionPane.ERROR_MESSAGE); 
                	 } else if (M > 12) {
                	 JOptionPane.showMessageDialog(frame, "Не трябва месеца да е по-голям от 12", "Грешка", JOptionPane.ERROR_MESSAGE); 
                	 } else if (D <= 0) {
                	 JOptionPane.showMessageDialog(frame, "Не трябва деня да е по-малък или равен на 0", "Грешка", JOptionPane.ERROR_MESSAGE); 
                	 } else if ((M == 1 || M == 3 || M == 5 || M == 7 || M == 8 || M == 10 || M == 12) && (D > 31)) {
                	 JOptionPane.showMessageDialog(frame, "Този месец има 31 дена", "Грешка", JOptionPane.ERROR_MESSAGE);
                	 } else if (( M == 4 || M == 6 || M == 9 || M == 11 ) && (D > 30)) {
                	 JOptionPane.showMessageDialog(frame, "Този месец има 30 дена", "Грешка", JOptionPane.ERROR_MESSAGE);
                	 } else if ((M == 2) && (D > 29)) {
                     JOptionPane.showMessageDialog(frame, "Този месец има 28 или 29 дена", "Грешка", JOptionPane.ERROR_MESSAGE);
                	 } else {
                		 
                        	 File reader = new File("filename1.txt");
                        	 String container = "";
                             Scanner myReader = new Scanner(reader); 
                             while (myReader.hasNextLine()) {
                                 String data = myReader.nextLine(); 
                                 System.out.println(data);
                                 if(data.contains("- "+ M +"/"+ D + " -" )) {
                                	 container  = container + "\n" + data;
                                 } 
                             } 
                             myReader.close();
                             if (container.equals("")) {
                             	JOptionPane.showMessageDialog(frame, "Тaкъв празник не е намерен", "Грешка", JOptionPane.ERROR_MESSAGE);
                             } else {
                            	 JOptionPane.showMessageDialog(frame, ""+ container, "", JOptionPane.INFORMATION_MESSAGE);
                             }
                               
                      	    } 
                      	 } 	catch (NumberFormatException f) {
                        	    JOptionPane.showMessageDialog(frame, "Въведете цифри, и то цели, а не букви!", "", JOptionPane.ERROR_MESSAGE);   	    
                         }  catch (FileNotFoundException f1)  {
                        	 JOptionPane.showMessageDialog(frame, "Файлът не е намерен", "", JOptionPane.ERROR_MESSAGE); 
                     }
        	     } 
             }
        });
        btnNewButton.setBackground(Color.WHITE);
        btnNewButton.setBounds(29, 131, 232, 52);
        panel.add(btnNewButton);
	        
	    frame.setVisible(true);
	        
	}
}
